package main

import "fmt"

const (
	parallelFalse bool = false
	parallelTrue  bool = true
	requestCount  int  = 1000000
)

func main() {
	fmt.Println("Hello Go!")
	calculateCount := StartWorker(requestCount, parallelTrue)
	calculateCount1 := StartWorker(requestCount, parallelFalse)
	fmt.Println(calculateCount1, calculateCount)
}
