package main

import (
	"fmt"
	"math/rand"
	"sync"
	"time"
)

var calculateCount int = 0

var wg sync.WaitGroup

func StartWorker(n int, parallel bool) int {

	calculateCount = 0

	start := time.Now()
	finish := time.Now()
	var result string

	if parallel {
		result = "parallel"
		resParallel := 0
		wg.Add(n)
		for i := 1; i <= n; i++ {
			calculateCount++
			go func() {
				resParallel += calculate(true)
			}()
		}
		wg.Wait()
		fmt.Println("resParallel", resParallel)
	} else {
		result = "sequence"
		resSequence := 0
		for i := 1; i <= n; i++ {
			calculateCount++
			resSequence += calculate(false)
		}
		fmt.Println("resSequence", resSequence)
	}

	finish = time.Now()
	fmt.Println(result, finish.Sub(start))

	return calculateCount

}

func calculate(parallel bool) int {

	// time.Sleep(2 * time.Second)

	result := 1
	randFirst := rand.Intn(100)
	randSecond := rand.Intn(100)
	result *= randFirst * randSecond
	if parallel {
		wg.Done()
	}
	return result

}
